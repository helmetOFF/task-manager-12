package ru.helmetoff.tm.service;

import ru.helmetoff.tm.api.repository.IProjectRepository;
import ru.helmetoff.tm.api.service.IProjectService;
import ru.helmetoff.tm.model.Project;
import ru.helmetoff.tm.model.Task;

import java.util.List;

public class ProjectService implements IProjectService {

    private IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(final String name) {
        Project project = new Project();
        project.setName(name);
        projectRepository.add(project);
    }

    @Override
    public void create(final String name, final String description) {
        Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
    }

    @Override
    public void add(final Project project) {
        if (project == null) return;
        projectRepository.add(project);
    }

    @Override
    public void remove(final Project project) {
        if (project == null) return;
        projectRepository.remove(project);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public Project findOneById(String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.findOneById(id);
    }

    @Override
    public Project findOneByIndex(Integer index) {
        if (index == null || index < 0) return null;
        return projectRepository.findOneByIndex(index);
    }

    @Override
    public Project findOneByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.findOneByName(name);
    }

    @Override
    public void removeOneById(String id) {
        if (id == null || id.isEmpty()) return;
        projectRepository.removeProjectById(id);
    }

    @Override
    public void removeOneByIndex(Integer index) {
        if (index == null || index < 0) return;
        projectRepository.removeProjectByIndex(index);
    }

    @Override
    public void removeOneByName(String name) {
        if (name == null || name.isEmpty()) return;
        projectRepository.removeProjectByName(name);
    }

    @Override
    public Project updateOneById(String id, String name, String description) {
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        final Project project = findOneById(id);
        if (project == null) return null;
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateOneByIndex(Integer index, String name, String description) {
        if (index == null || index.intValue() < 0) return null;
        if (name == null || name.isEmpty()) return null;
        final Project project = findOneByIndex(index);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }
}
