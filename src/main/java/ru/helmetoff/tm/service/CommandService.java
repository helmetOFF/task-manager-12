package ru.helmetoff.tm.service;

import ru.helmetoff.tm.api.repository.ICommandRepository;
import ru.helmetoff.tm.api.service.ICommandService;
import ru.helmetoff.tm.model.Command;

public class CommandService implements ICommandService {

    private ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    public String[] getCommands() {
        return commandRepository.getCommands();
    }

    public String[] getArguments() {
        return commandRepository.getArguments();
    }

}
