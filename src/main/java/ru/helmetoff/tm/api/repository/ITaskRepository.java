package ru.helmetoff.tm.api.repository;

import ru.helmetoff.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    void remove(Task task);

    List<Task> findAll();

    void clear();

    Task findOneByIndex(Integer index);

    Task findOneById(String id);

    Task findOneByName(String name);

    void removeTaskByIndex(Integer index);

    void removeTaskById(String id);

    void removeTaskByName(String name);

}
