package ru.helmetoff.tm.api.controller;

import ru.helmetoff.tm.model.Project;

public interface IProjectController {

    void showProjects();

    void showProject(Project project);

    void clearProjects();

    void createProject();

    void removeOneByIndex();

    void removeOneById();

    void removeOneByName();

    void viewOneByIndex();

    void viewOneById();

    void viewOneByName();

    void updateOneByIndex();

    void updateOneById();

}
