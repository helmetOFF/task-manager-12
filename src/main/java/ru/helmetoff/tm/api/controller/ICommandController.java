package ru.helmetoff.tm.api.controller;

public interface ICommandController {

    void displayHelp();

    void displayCommands();

    void displayArguments();

    void displayVersion();

    void displayAbout();

    void displayInfo();

    void displayWrongCommand(final String arg);

    void exit();

}
