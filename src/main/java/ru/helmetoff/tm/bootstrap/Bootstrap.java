package ru.helmetoff.tm.bootstrap;

import ru.helmetoff.tm.api.controller.ICommandController;
import ru.helmetoff.tm.api.controller.IProjectController;
import ru.helmetoff.tm.api.controller.ITaskController;
import ru.helmetoff.tm.api.repository.ICommandRepository;
import ru.helmetoff.tm.api.repository.IProjectRepository;
import ru.helmetoff.tm.api.repository.ITaskRepository;
import ru.helmetoff.tm.api.service.ICommandService;
import ru.helmetoff.tm.api.service.IProjectService;
import ru.helmetoff.tm.api.service.ITaskService;
import ru.helmetoff.tm.constant.ArgumentConst;
import ru.helmetoff.tm.constant.TerminalConst;
import ru.helmetoff.tm.controller.CommandController;
import ru.helmetoff.tm.controller.ProjectController;
import ru.helmetoff.tm.controller.TaskController;
import ru.helmetoff.tm.repository.CommandRepository;
import ru.helmetoff.tm.repository.ProjectRepository;
import ru.helmetoff.tm.repository.TaskRepository;
import ru.helmetoff.tm.service.CommandService;
import ru.helmetoff.tm.service.ProjectService;
import ru.helmetoff.tm.service.TaskService;
import ru.helmetoff.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    public void run(String... args) {
        displayWelcome();
        if (parseArgs(args)) System.exit(0);
        while (true) parseCommand(TerminalUtil.nextLine());
    }

    private void parseCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalConst.HELP:
                commandController.displayHelp();
                break;
            case TerminalConst.VERSION:
                commandController.displayVersion();
                break;
            case TerminalConst.ABOUT:
                commandController.displayAbout();
                break;
            case TerminalConst.INFO:
                commandController.displayInfo();
                break;
            case TerminalConst.EXIT:
                commandController.exit();
                break;
            case TerminalConst.COMMANDS:
                commandController.displayCommands();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.displayArguments();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTasks();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case TerminalConst.TASK_REMOVE_BY_INDEX:
                taskController.removeOneByIndex();
                break;
            case TerminalConst.TASK_REMOVE_BY_ID:
                taskController.removeOneById();
                break;
            case TerminalConst.TASK_REMOVE_BY_NAME:
                taskController.removeOneByName();
                break;
            case TerminalConst.TASK_VIEW_BY_INDEX:
                taskController.viewOneByIndex();
                break;
            case TerminalConst.TASK_VIEW_BY_ID:
                taskController.viewOneById();
                break;
            case TerminalConst.TASK_VIEW_BY_NAME:
                taskController.viewOneByName();
                break;
            case TerminalConst.TASK_UPDATE_BY_INDEX:
                taskController.updateOneByIndex();
                break;
            case TerminalConst.TASK_UPDATE_BY_ID:
                taskController.updateOneById();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeOneByIndex();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_ID:
                projectController.removeOneById();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_NAME:
                projectController.removeOneByName();
                break;
            case TerminalConst.PROJECT_VIEW_BY_INDEX:
                projectController.viewOneByIndex();
                break;
            case TerminalConst.PROJECT_VIEW_BY_ID:
                projectController.viewOneById();
                break;
            case TerminalConst.PROJECT_VIEW_BY_NAME:
                projectController.viewOneByName();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateOneByIndex();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_ID:
                projectController.updateOneById();
                break;
            default:
                commandController.displayWrongCommand(command);
                break;
        }
    }

    private void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.HELP:
                commandController.displayHelp();
                break;
            case ArgumentConst.VERSION:
                commandController.displayVersion();
                break;
            case ArgumentConst.ABOUT:
                commandController.displayAbout();
                break;
            case ArgumentConst.INFO:
                commandController.displayInfo();
                break;
            case ArgumentConst.COMMANDS:
                commandController.displayCommands();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.displayArguments();
                break;
            default:
                commandController.displayWrongCommand(arg);
                break;
        }
    }

    private boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

}
