package ru.helmetoff.tm.controller;

import ru.helmetoff.tm.api.controller.ICommandController;
import ru.helmetoff.tm.api.service.ICommandService;
import ru.helmetoff.tm.repository.IAppInfoRepository;
import ru.helmetoff.tm.model.Command;
import ru.helmetoff.tm.util.NumberUtil;

import java.util.Arrays;

public class CommandController implements ICommandController {

    private ICommandService CommandService;

    public CommandController(ICommandService CommandService) {
        this.CommandService = CommandService;
    }

    public void displayHelp() {
        System.out.println("[HELP]");
        final Command[] commands = CommandService.getTerminalCommands();
        for (final Command command : commands)
            System.out.println(command);
    }

    public void displayCommands() {
        final String[] commands = CommandService.getCommands();
        System.out.println(Arrays.toString(commands));
    }

    public void displayArguments() {
        final String[] arguments = CommandService.getArguments();
        System.out.println(Arrays.toString(arguments));
    }

    public void displayVersion() {
        System.out.println("[VERSION]");
        System.out.println(IAppInfoRepository.VERSION);
    }

    public void displayAbout() {
        System.out.println("ABOUT");
        System.out.println("NAME: " + IAppInfoRepository.DEVELOPER_NAME);
        System.out.println("E-MAIL: " + IAppInfoRepository.DEVELOPER_EMAIL);
    }

    public void displayInfo() {
        System.out.println("[INFO]");

        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);

        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = (maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue);
        System.out.println("Maximum memory: " + maxMemoryFormat);

        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM: " + totalMemoryFormat);

        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));

    }

    public void displayWrongCommand(final String command) {
        System.out.println(String.format("\"%s\" is invalid command.", command));
    }

    public void exit() {
        System.exit(0);
    }

}
