package ru.helmetoff.tm.repository;

public interface IAppInfoRepository {

    String VERSION = "0.12.0";

    String DEVELOPER_NAME = "Vladislav Halmetov";

    String DEVELOPER_EMAIL = "halmetoff@gmail.com";

}
